#ifndef _CPA_H_
#define _CPA_H_
#include <stddef.h>
#include <stdint.h>
#include "traces.h"

extern void correlationCoefficient(double traces[NB_TRACES][NB_POINTS], uint8_t hw_text_in[NB_TRACES][NB_BYTES_IN_TEXT][NB_GUESS]);

extern int fill_hw_with_text_in(uint8_t text_arr[NB_TRACES][NB_BYTES_IN_TEXT], uint8_t  hw_arr[NB_TRACES][NB_BYTES_IN_TEXT][NB_GUESS], size_t nb_traces);

extern uint8_t getSboxVal(uint8_t id);
extern uint8_t getHWVal(uint8_t var_8_bit);
extern uint8_t intermediate(uint8_t pt, uint8_t keyguess);

#endif //! _CPA_H_