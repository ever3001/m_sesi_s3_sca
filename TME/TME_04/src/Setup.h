#ifndef _SETUP_H_
#define _SETUP_H_

#define NB_TRACES           (100)
#define NB_POINTS           (1080)

#define TEXT_IN_FILE_PATH   ("text/text_in.raw")
#define KEY_IN_FILE_PATH    ("key/key.raw")
#define TRACES_IN_FILE_PATH ("traces/traces.raw")

#endif //! _SETUP_H_