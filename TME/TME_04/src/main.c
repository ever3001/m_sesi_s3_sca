#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "CPA.h"

// ! PLEASE EDIT THE FILE Setup.h BEFORE COMPILE THE CODE

int main(int argc, char const *argv[]) {
  // Variables for counting the time of the function
  clock_t start, end;
  double cpu_time_used;

  // Read and fill the array with the key.
  fill_array_with_raw_file_uint8(key, NB_BYTES_IN_KEY, KEY_IN_FILE_PATH);
  print_key();
  // Read and fill the array with all the input text of the traces
  fill_array_with_raw_file_uint8(text_in[0], NB_TRACES * NB_BYTES_IN_TEXT, TEXT_IN_FILE_PATH);
  print_text(0);
  // Read and fill the array with all the points of each trace
  fill_array_with_raw_file_double(traces[0], NB_TRACES * NB_POINTS, TRACES_IN_FILE_PATH);
  print_trace(0, 10);
  // Print the number of traces and the number of points
  //! THESE VALUES MUST MATCH WITH THE PYTHON CODE
  printf("Number of traces               = %6d\r\n", NB_TRACES);
  printf("Number of points in each trace = %6d\r\n", NB_POINTS);
  // Init the chronometer
  start = clock();
  // Calculate the Hamming Weight of each input value with every guess key
  fill_hw_with_text_in(text_in, hw_text_in, NB_TRACES);
  // Calculate Pearson correlation coefficient for each trace, point and HW
  correlationCoefficient(traces, hw_text_in);
  // Stop the chronometer
  end = clock();
  // Calculate the time in sec
  cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
  // Print the final key
  print_key();
  // Print the execution time
  printf("The execution took %f seconds\r\n", cpu_time_used);
  return 0;
}
