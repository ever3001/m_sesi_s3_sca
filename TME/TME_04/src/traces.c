#include "traces.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

double traces[NB_TRACES][NB_POINTS] = {0};
uint8_t key[NB_BYTES_IN_KEY] = {0};
uint8_t text_in[NB_TRACES][NB_BYTES_IN_TEXT] = {0};
uint8_t hw_text_in[NB_TRACES][NB_BYTES_IN_TEXT][NB_GUESS] = {0};

int fill_array_with_raw_file_uint8(uint8_t *arr, size_t sz,
                                   const char *pathfile) {
  int fd;
  if ((fd = open(pathfile, O_RDONLY)) == -1) {
    perror("open");
    return -1;
  }
  if (read(fd, arr, sizeof(uint8_t) * sz) == -1) {
    perror("read");
    return -2;
  }
  close(fd);
  return 0;
}

int fill_array_with_raw_file_double(double *arr, size_t sz,
                                    const char *pathfile) {
  int fd;
  if ((fd = open(pathfile, O_RDONLY)) == -1) {
    perror("open");
    return -1;
  }
  if (read(fd, arr, sizeof(double) * sz) == -1) {
    perror("read");
    return -2;
  }
  close(fd);
  return 0;
}

void print_key() {
  size_t i;
  printf("The Key in file is : ");
  for (i = 0; i < NB_BYTES_IN_KEY; ++i) {
    printf("0x%02x ", key[i]);
  }
  printf("\r\n");
}
void print_text(uint16_t row) {
  size_t i;
  for (i = 0; i < NB_BYTES_IN_TEXT; ++i) {
    printf("0x%02x ", text_in[row][i]);
  }
  printf("\r\n");
}
void print_trace(uint16_t row, uint16_t nb_samples_to_print) {
  size_t i;
  for (i = 0; i < nb_samples_to_print; ++i) {
    printf("%lf\r\n", traces[row][i]);
  }
  printf("\r\n");
}