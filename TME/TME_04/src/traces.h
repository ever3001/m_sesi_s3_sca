#ifndef _TRACES_H_
#define _TRACES_H_

#include <stdint.h>
#include <stddef.h>

#include "Setup.h"

#define NB_BYTES_IN_KEY   (16)
#define NB_BYTES_IN_TEXT  NB_BYTES_IN_KEY

#define NB_GUESS (256)

extern double   traces[NB_TRACES][NB_POINTS];
extern uint8_t  key[NB_BYTES_IN_KEY];
extern uint8_t  text_in[NB_TRACES][NB_BYTES_IN_TEXT];
extern uint8_t  hw_text_in[NB_TRACES][NB_BYTES_IN_TEXT][NB_GUESS];

extern int fill_array_with_raw_file_uint8(uint8_t *arr, size_t sz, const char *pathfile);
extern int fill_array_with_raw_file_double(double *arr, size_t sz, const char *pathfile);

extern void print_key();
extern void print_text(uint16_t row);
extern void print_trace(uint16_t row, uint16_t nb_samples_to_print);



#endif //! _TRACES_H_